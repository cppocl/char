/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../CharIsAscii.hpp"

TEST_MEMBER_FUNCTION(CharIsAscii, IsAscii, char)
{
    typedef char char_type;
    typedef ocl::CharIsAscii<char_type> char_is_ascii_type;

    CHECK_TRUE(char_is_ascii_type::IsAscii('\0'));
    CHECK_TRUE(char_is_ascii_type::IsAscii('\x7f'));

    if (char_is_ascii_type::is_signed)
        CHECK_FALSE(char_is_ascii_type::IsAscii(static_cast<char>(-1)));
}

TEST_MEMBER_FUNCTION(CharIsAscii, IsAscii, wchar_t)
{
    typedef wchar_t char_type;
    typedef ocl::CharIsAscii<char_type> char_is_ascii_type;

    CHECK_TRUE(char_is_ascii_type::IsAscii('\0'));
    CHECK_TRUE(char_is_ascii_type::IsAscii('\x7f'));

    CHECK_FALSE(char_is_ascii_type::IsAscii(L'\u0080'));
    CHECK_FALSE(char_is_ascii_type::IsAscii(L'\u0101'));
}

TEST_MEMBER_FUNCTION(CharIsAscii, IsAscii, char16_t)
{
    typedef char16_t char_type;
    typedef ocl::CharIsAscii<char_type> char_is_ascii_type;

    CHECK_TRUE(char_is_ascii_type::IsAscii('\0'));
    CHECK_FALSE(char_is_ascii_type::IsAscii(u'\u0080'));
    CHECK_FALSE(char_is_ascii_type::IsAscii(u'\u0101'));
}

TEST_MEMBER_FUNCTION(CharIsAscii, IsAscii, char32_t)
{
    typedef char32_t char_type;
    typedef ocl::CharIsAscii<char_type> char_is_ascii_type;

    CHECK_TRUE(char_is_ascii_type::IsAscii('\0'));
    CHECK_FALSE(char_is_ascii_type::IsAscii(U'\u0080'));
    CHECK_FALSE(char_is_ascii_type::IsAscii(U'\u0101'));
}
