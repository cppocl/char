/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_CHAR_CHARISASCII_HPP
#define OCL_GUARD_CHAR_CHARISASCII_HPP

namespace ocl
{

template<typename CharType>
struct CharIsAscii;

template<>
struct CharIsAscii<char>
{
    typedef char char_type;

    // char type can be overridden to be unsigned by the compiler,
    // so handle signed and unsigned chars.
    static bool const is_signed = static_cast<char>(-1) < static_cast<char>(0);

    static bool constexpr IsAscii(char_type const ch) noexcept
    {
        return is_signed ? ch >= '\0' : ch <= '\x7f';
    }
};

template<>
struct CharIsAscii<wchar_t>
{
    typedef wchar_t char_type;

    static bool constexpr IsAscii(char_type const ch) noexcept
    {
        return ch <= L'\x7f';
    }
};

template<>
struct CharIsAscii<char16_t>
{
    typedef char16_t char_type;

    static bool constexpr IsAscii(char_type const ch) noexcept
    {
        return ch <= u'\x7f';
    }
};

template<>
struct CharIsAscii<char32_t>
{
    typedef char32_t char_type;

    static bool constexpr IsAscii(char_type const ch) noexcept
    {
        return ch <= U'\x7f';
    }
};

} // namespace ocl

#endif // OCL_GUARD_CHAR_CHARISASCII_HPP
